import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { ormConfig } from './ormconfig';

const connectDb: any = ormConfig

@Module({
  imports: [TypeOrmModule.forRoot(connectDb)],
  providers: [],
  exports: []
})
export class ConfigModule {}
