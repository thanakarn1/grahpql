
require('dotenv').config()
const connection = process.env.DB_CONNECTION || 'mysql'
const host = process.env.DB_HOST || 'db'
const port = process.env.DB_PORT || 33061
const dbName = process.env.DB_DATABASE || 'gplatform'
const username = process.env.DB_USERNAME || 'root'
const password = process.env.DB_PASSWORD || 'root'

export const ormConfig ={
  type: connection,
  host: host,
  port: port,
  username: username,
  password: password,
  database: dbName,
  synchronize: false,
  logging: false,
  logger: 'file',
  entities: [__dirname + '/../**/**/*.entity{.ts,.js}'],
  migrations: [__dirname + '/../database/migration/*{.ts,.js}'],
  subscribers: [__dirname + '/../**/**/*.subscriber{.ts,.js}'],
  cli: {
    migrationsDir: 'src/database/migration'
  }
}
 