import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './user.entity';
import { Repository } from 'typeorm';
import { UserDto } from './user.dto';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {}

  create(details: UserDto): Promise<User> {
    return this.userRepository.save(details);
  }

  findAll(): Promise<User[]> {
    return this.userRepository.find();
  }

  findOne(id: string): Promise<User> {
    return this.userRepository.findOne(id);
  }
  async update(id: string, details: UserDto) {
    return await this.userRepository.update(id, details);
  }

  delete(id: number) {
    return this.userRepository.delete(id);
  }
}
