import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { UserService } from './user.service';
import { Inject } from '@nestjs/common';
import { User } from './user.entity';
import { async } from 'rxjs';

@Resolver('User')
export class UserResolver {
  constructor(@Inject(UserService) private userService: UserService) {}

  @Query(returns => User)
  async user(@Args('id') id: string): Promise<User> {
    return await this.userService.findOne(id);
  }
  @Query(returns => [User])
  async users(): Promise<User[]> {
    return await this.userService.findAll();
  }

  @Mutation(returns => User)
  async createUser(
    @Args('name') name: string,
    @Args('email') email: string,
    @Args('phone', { nullable: true }) phone: string,
    @Args('address', { nullable: true }) address: string,
  ): Promise<User> {
    return await this.userService.create({ name, email, phone, address });
  }

  @Mutation(() => String)
  async delete(@Args('id') id: number) {
    try {
      await this.userService.delete(id);
      return 'Delete Successful';
    } catch (error) {
      return error;
    }
  }

  @Mutation(returns => User)
  async updateUser(
    @Args('id') id: string,
    @Args('name') name: string,
    @Args('email') email: string,
    @Args('phone', { nullable: true }) phone: string,
    @Args('address', { nullable: true }) address: string,
  ): Promise<User> {
    this.userService.update(id, { name, email, phone, address });
    return await this.userService.findOne(id);
  }
}
