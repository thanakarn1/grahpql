export declare namespace noteFields {
    type title = string;
    type description = string | null;
    type id = number;
}
export interface note {
    title: noteFields.title;
    description: noteFields.description;
    id: noteFields.id;
}
export declare namespace commentFields {
    type text = string | null;
    type description = string | null;
    type noteId = number | null;
    type id = number;
}
export interface comment {
    text: commentFields.text;
    description: commentFields.description;
    noteId: commentFields.noteId;
    id: commentFields.id;
}
