import { GraphQLResolveInfo } from 'graphql';
import { comment, note } from './generated-db-types';
import { GraphbackRuntimeContext } from '@graphback/runtime';
export declare type Maybe<T> = T | null;
export declare type RequireFields<T, K extends keyof T> = {
    [X in Exclude<keyof T, K>]?: T[X];
} & {
    [P in K]-?: NonNullable<T[P]>;
};
export declare type Scalars = {
    ID: string;
    String: string;
    Boolean: boolean;
    Int: number;
    Float: number;
};
export declare type Comment = {
    __typename?: 'Comment';
    id: Scalars['ID'];
    text?: Maybe<Scalars['String']>;
    description?: Maybe<Scalars['String']>;
    note?: Maybe<Note>;
};
export declare type CommentInput = {
    id?: Maybe<Scalars['ID']>;
    text?: Maybe<Scalars['String']>;
    description?: Maybe<Scalars['String']>;
    noteId?: Maybe<Scalars['ID']>;
};
export declare type Mutation = {
    __typename?: 'Mutation';
    createNote: Note;
    updateNote: Note;
    deleteNote: Note;
    createComment: Comment;
    updateComment: Comment;
    deleteComment: Comment;
};
export declare type MutationCreateNoteArgs = {
    input?: Maybe<NoteInput>;
};
export declare type MutationUpdateNoteArgs = {
    input?: Maybe<NoteInput>;
};
export declare type MutationDeleteNoteArgs = {
    input?: Maybe<NoteInput>;
};
export declare type MutationCreateCommentArgs = {
    input?: Maybe<CommentInput>;
};
export declare type MutationUpdateCommentArgs = {
    input?: Maybe<CommentInput>;
};
export declare type MutationDeleteCommentArgs = {
    input?: Maybe<CommentInput>;
};
export declare type Note = {
    __typename?: 'Note';
    id: Scalars['ID'];
    title: Scalars['String'];
    description?: Maybe<Scalars['String']>;
    comments: Array<Maybe<Comment>>;
};
export declare type NoteInput = {
    id?: Maybe<Scalars['ID']>;
    title?: Maybe<Scalars['String']>;
    description?: Maybe<Scalars['String']>;
};
export declare type Query = {
    __typename?: 'Query';
    findAllNotes: Array<Maybe<Note>>;
    findNotes: Array<Maybe<Note>>;
    findAllComments: Array<Maybe<Comment>>;
    findComments: Array<Maybe<Comment>>;
};
export declare type QueryFindAllNotesArgs = {
    limit?: Maybe<Scalars['Int']>;
    offset?: Maybe<Scalars['Int']>;
};
export declare type QueryFindNotesArgs = {
    fields?: Maybe<NoteInput>;
    limit?: Maybe<Scalars['Int']>;
    offset?: Maybe<Scalars['Int']>;
};
export declare type QueryFindAllCommentsArgs = {
    limit?: Maybe<Scalars['Int']>;
    offset?: Maybe<Scalars['Int']>;
};
export declare type QueryFindCommentsArgs = {
    fields?: Maybe<CommentInput>;
    limit?: Maybe<Scalars['Int']>;
    offset?: Maybe<Scalars['Int']>;
};
export declare type Subscription = {
    __typename?: 'Subscription';
    newNote: Note;
    updatedNote: Note;
    deletedNote: Note;
    newComment: Comment;
    updatedComment: Comment;
    deletedComment: Comment;
};
export declare type SubscriptionNewNoteArgs = {
    input?: Maybe<NoteInput>;
};
export declare type SubscriptionUpdatedNoteArgs = {
    input?: Maybe<NoteInput>;
};
export declare type SubscriptionDeletedNoteArgs = {
    input?: Maybe<NoteInput>;
};
export declare type SubscriptionNewCommentArgs = {
    input?: Maybe<CommentInput>;
};
export declare type SubscriptionUpdatedCommentArgs = {
    input?: Maybe<CommentInput>;
};
export declare type SubscriptionDeletedCommentArgs = {
    input?: Maybe<CommentInput>;
};
export declare type WithIndex<TObject> = TObject & Record<string, any>;
export declare type ResolversObject<TObject> = WithIndex<TObject>;
export declare type ResolverTypeWrapper<T> = Promise<T> | T;
export declare type StitchingResolver<TResult, TParent, TContext, TArgs> = {
    fragment: string;
    resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};
export declare type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> = ResolverFn<TResult, TParent, TContext, TArgs> | StitchingResolver<TResult, TParent, TContext, TArgs>;
export declare type ResolverFn<TResult, TParent, TContext, TArgs> = (parent: TParent, args: TArgs, context: TContext, info: GraphQLResolveInfo) => Promise<TResult> | TResult;
export declare type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (parent: TParent, args: TArgs, context: TContext, info: GraphQLResolveInfo) => AsyncIterator<TResult> | Promise<AsyncIterator<TResult>>;
export declare type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (parent: TParent, args: TArgs, context: TContext, info: GraphQLResolveInfo) => TResult | Promise<TResult>;
export interface SubscriptionSubscriberObject<TResult, TKey extends string, TParent, TContext, TArgs> {
    subscribe: SubscriptionSubscribeFn<{
        [key in TKey]: TResult;
    }, TParent, TContext, TArgs>;
    resolve?: SubscriptionResolveFn<TResult, {
        [key in TKey]: TResult;
    }, TContext, TArgs>;
}
export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
    subscribe: SubscriptionSubscribeFn<any, TParent, TContext, TArgs>;
    resolve: SubscriptionResolveFn<TResult, any, TContext, TArgs>;
}
export declare type SubscriptionObject<TResult, TKey extends string, TParent, TContext, TArgs> = SubscriptionSubscriberObject<TResult, TKey, TParent, TContext, TArgs> | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>;
export declare type SubscriptionResolver<TResult, TKey extends string, TParent = {}, TContext = {}, TArgs = {}> = ((...args: any[]) => SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>) | SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>;
export declare type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (parent: TParent, context: TContext, info: GraphQLResolveInfo) => Maybe<TTypes> | Promise<Maybe<TTypes>>;
export declare type IsTypeOfResolverFn<T = {}> = (obj: T, info: GraphQLResolveInfo) => boolean | Promise<boolean>;
export declare type NextResolverFn<T> = () => Promise<T>;
export declare type DirectiveResolverFn<TResult = {}, TParent = {}, TContext = {}, TArgs = {}> = (next: NextResolverFn<TResult>, parent: TParent, args: TArgs, context: TContext, info: GraphQLResolveInfo) => TResult | Promise<TResult>;
export declare type ResolversTypes = ResolversObject<{
    String: ResolverTypeWrapper<Scalars['String']>;
    Boolean: ResolverTypeWrapper<Scalars['Boolean']>;
    Comment: ResolverTypeWrapper<comment>;
    ID: ResolverTypeWrapper<Scalars['ID']>;
    CommentInput: CommentInput;
    Mutation: ResolverTypeWrapper<{}>;
    Note: ResolverTypeWrapper<note>;
    NoteInput: NoteInput;
    Query: ResolverTypeWrapper<{}>;
    Int: ResolverTypeWrapper<Scalars['Int']>;
    Subscription: ResolverTypeWrapper<{}>;
}>;
export declare type ResolversParentTypes = ResolversObject<{
    String: Scalars['String'];
    Boolean: Scalars['Boolean'];
    Comment: comment;
    ID: Scalars['ID'];
    CommentInput: CommentInput;
    Mutation: {};
    Note: note;
    NoteInput: NoteInput;
    Query: {};
    Int: Scalars['Int'];
    Subscription: {};
}>;
export declare type CommentResolvers<ContextType = GraphbackRuntimeContext, ParentType extends ResolversParentTypes['Comment'] = ResolversParentTypes['Comment']> = ResolversObject<{
    id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
    text?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
    description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
    note?: Resolver<Maybe<ResolversTypes['Note']>, ParentType, ContextType>;
    __isTypeOf?: IsTypeOfResolverFn<ParentType>;
}>;
export declare type MutationResolvers<ContextType = GraphbackRuntimeContext, ParentType extends ResolversParentTypes['Mutation'] = ResolversParentTypes['Mutation']> = ResolversObject<{
    createNote?: Resolver<ResolversTypes['Note'], ParentType, ContextType, RequireFields<MutationCreateNoteArgs, never>>;
    updateNote?: Resolver<ResolversTypes['Note'], ParentType, ContextType, RequireFields<MutationUpdateNoteArgs, never>>;
    deleteNote?: Resolver<ResolversTypes['Note'], ParentType, ContextType, RequireFields<MutationDeleteNoteArgs, never>>;
    createComment?: Resolver<ResolversTypes['Comment'], ParentType, ContextType, RequireFields<MutationCreateCommentArgs, never>>;
    updateComment?: Resolver<ResolversTypes['Comment'], ParentType, ContextType, RequireFields<MutationUpdateCommentArgs, never>>;
    deleteComment?: Resolver<ResolversTypes['Comment'], ParentType, ContextType, RequireFields<MutationDeleteCommentArgs, never>>;
}>;
export declare type NoteResolvers<ContextType = GraphbackRuntimeContext, ParentType extends ResolversParentTypes['Note'] = ResolversParentTypes['Note']> = ResolversObject<{
    id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
    title?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
    description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
    comments?: Resolver<Array<Maybe<ResolversTypes['Comment']>>, ParentType, ContextType>;
    __isTypeOf?: IsTypeOfResolverFn<ParentType>;
}>;
export declare type QueryResolvers<ContextType = GraphbackRuntimeContext, ParentType extends ResolversParentTypes['Query'] = ResolversParentTypes['Query']> = ResolversObject<{
    findAllNotes?: Resolver<Array<Maybe<ResolversTypes['Note']>>, ParentType, ContextType, RequireFields<QueryFindAllNotesArgs, never>>;
    findNotes?: Resolver<Array<Maybe<ResolversTypes['Note']>>, ParentType, ContextType, RequireFields<QueryFindNotesArgs, never>>;
    findAllComments?: Resolver<Array<Maybe<ResolversTypes['Comment']>>, ParentType, ContextType, RequireFields<QueryFindAllCommentsArgs, never>>;
    findComments?: Resolver<Array<Maybe<ResolversTypes['Comment']>>, ParentType, ContextType, RequireFields<QueryFindCommentsArgs, never>>;
}>;
export declare type SubscriptionResolvers<ContextType = GraphbackRuntimeContext, ParentType extends ResolversParentTypes['Subscription'] = ResolversParentTypes['Subscription']> = ResolversObject<{
    newNote?: SubscriptionResolver<ResolversTypes['Note'], "newNote", ParentType, ContextType, RequireFields<SubscriptionNewNoteArgs, never>>;
    updatedNote?: SubscriptionResolver<ResolversTypes['Note'], "updatedNote", ParentType, ContextType, RequireFields<SubscriptionUpdatedNoteArgs, never>>;
    deletedNote?: SubscriptionResolver<ResolversTypes['Note'], "deletedNote", ParentType, ContextType, RequireFields<SubscriptionDeletedNoteArgs, never>>;
    newComment?: SubscriptionResolver<ResolversTypes['Comment'], "newComment", ParentType, ContextType, RequireFields<SubscriptionNewCommentArgs, never>>;
    updatedComment?: SubscriptionResolver<ResolversTypes['Comment'], "updatedComment", ParentType, ContextType, RequireFields<SubscriptionUpdatedCommentArgs, never>>;
    deletedComment?: SubscriptionResolver<ResolversTypes['Comment'], "deletedComment", ParentType, ContextType, RequireFields<SubscriptionDeletedCommentArgs, never>>;
}>;
export declare type Resolvers<ContextType = GraphbackRuntimeContext> = ResolversObject<{
    Comment?: CommentResolvers<ContextType>;
    Mutation?: MutationResolvers<ContextType>;
    Note?: NoteResolvers<ContextType>;
    Query?: QueryResolvers<ContextType>;
    Subscription?: SubscriptionResolvers<ContextType>;
}>;
export declare type IResolvers<ContextType = GraphbackRuntimeContext> = Resolvers<ContextType>;
