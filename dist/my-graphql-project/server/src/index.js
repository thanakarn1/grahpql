"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const cors_1 = require("cors");
const express_1 = require("express");
const http_1 = require("http");
const graphql_1 = require("./graphql");
async function start() {
    const app = express_1.default();
    app.use(cors_1.default());
    app.get('/health', (req, res) => res.sendStatus(200));
    const apolloServer = await graphql_1.createApolloServer();
    apolloServer.applyMiddleware({ app });
    const httpServer = http_1.default.createServer(app);
    apolloServer.installSubscriptionHandlers(httpServer);
    const port = process.env.PORT || 4000;
    httpServer.listen({ port }, () => {
        console.log(`🚀  Server ready at http://localhost:${port}/graphql`);
    });
}
start().catch((err) => {
    console.error(err);
    process.exit(1);
});
//# sourceMappingURL=index.js.map