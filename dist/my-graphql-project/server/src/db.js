"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createDB = exports.getConfig = void 0;
const graphql_config_1 = require("graphql-config");
const knex_1 = require("knex");
exports.getConfig = async () => {
    const config = await graphql_config_1.loadConfig({
        extensions: [() => ({ name: 'dbmigrations' })],
    });
    if (!config) {
        throw new Error('Missing dbmigrations config');
    }
    const conf = await config.getDefault().extension('dbmigrations');
    return conf;
};
exports.createDB = async () => {
    const dbmigrations = await exports.getConfig();
    const db = knex_1.default(dbmigrations);
    return db;
};
//# sourceMappingURL=db.js.map