"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createApolloServer = void 0;
const path_1 = require("path");
const load_files_1 = require("@graphql-tools/load-files");
const apollo_server_express_1 = require("apollo-server-express");
const graphql_1 = require("graphql");
const runtime_knex_1 = require("@graphback/runtime-knex");
const db_1 = require("./db");
const models_1 = require("./resolvers/models");
const resolvers_1 = require("./resolvers/resolvers");
exports.createApolloServer = async () => {
    const db = await db_1.createDB();
    const pubSub = new apollo_server_express_1.PubSub();
    const typeDefs = (await load_files_1.loadFiles(path_1.join(__dirname, '/schema/'))).join('\n');
    const schema = graphql_1.buildSchema(typeDefs);
    const context = runtime_knex_1.createKnexPGCRUDRuntimeServices(models_1.models, schema, db, pubSub);
    const apolloServer = new apollo_server_express_1.ApolloServer({
        typeDefs: typeDefs,
        resolvers: resolvers_1.default,
        context,
        playground: true,
    });
    return apolloServer;
};
//# sourceMappingURL=graphql.js.map