export declare const models: {
    name: string;
    pubSub: {
        publishCreate: boolean;
        publishUpdate: boolean;
        publishDelete: boolean;
    };
}[];
