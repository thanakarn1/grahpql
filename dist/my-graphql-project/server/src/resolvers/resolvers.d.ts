declare const _default: {
    Note: {
        comments: (parent: any, args: any, context: any) => any;
    };
    Comment: {
        note: (parent: any, args: any, context: any) => any;
    };
    Query: {
        findNotes: (parent: any, args: any, context: any) => any;
        findAllNotes: (parent: any, args: any, context: any) => any;
        findComments: (parent: any, args: any, context: any) => any;
        findAllComments: (parent: any, args: any, context: any) => any;
    };
    Mutation: {
        createNote: (parent: any, args: any, context: any) => any;
        updateNote: (parent: any, args: any, context: any) => any;
        deleteNote: (parent: any, args: any, context: any) => any;
        createComment: (parent: any, args: any, context: any) => any;
        updateComment: (parent: any, args: any, context: any) => any;
        deleteComment: (parent: any, args: any, context: any) => any;
    };
    Subscription: {
        newNote: {
            subscribe: (parent: any, args: any, context: any) => any;
        };
        updatedNote: {
            subscribe: (parent: any, args: any, context: any) => any;
        };
        deletedNote: {
            subscribe: (parent: any, args: any, context: any) => any;
        };
        newComment: {
            subscribe: (parent: any, args: any, context: any) => any;
        };
        updatedComment: {
            subscribe: (parent: any, args: any, context: any) => any;
        };
        deletedComment: {
            subscribe: (parent: any, args: any, context: any) => any;
        };
    };
};
export default _default;
