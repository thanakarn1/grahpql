"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.models = void 0;
exports.models = [
    {
        name: "Note",
        pubSub: {
            publishCreate: true,
            publishUpdate: true,
            publishDelete: true,
        },
    },
    {
        name: "Comment",
        pubSub: {
            publishCreate: true,
            publishUpdate: true,
            publishDelete: true,
        },
    },
];
//# sourceMappingURL=models.js.map