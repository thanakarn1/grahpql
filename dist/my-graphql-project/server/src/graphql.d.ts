import { ApolloServer } from 'apollo-server-express';
export declare const createApolloServer: () => Promise<ApolloServer>;
