"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = require("react");
const generated_types_1 = require("../../generated-types");
const core_1 = require("@material-ui/core");
require("./Note.css");
const EditNote = ({ id, title, description, editState }) => {
    const [updateNote] = generated_types_1.useUpdateNoteMutation();
    const [NoteTitle, setNoteTitle] = react_1.useState(title);
    const [NoteDescription, setNoteDescription] = react_1.useState(description);
    return (<div>
      <core_1.Card className="inputCard">
        <form noValidate autoComplete="off" className="inputForm">
          <h3>Edit Note</h3>
          <core_1.TextField label="Title" variant="outlined" onChange={(e) => setNoteTitle(e.target.value)} value={NoteTitle}/>
          <core_1.TextField label="Description" variant="outlined" onChange={(e) => setNoteDescription(e.target.value)} value={NoteDescription}/>
          <core_1.Button variant="outlined" color="primary" onClick={() => {
        updateNote({ variables: { input: { id: id, title: NoteTitle, description: NoteDescription } } });
        editState(false);
    }}>
            Update Note
          </core_1.Button>
        </form>
      </core_1.Card>
    </div>);
};
exports.default = EditNote;
//# sourceMappingURL=EditNote.js.map