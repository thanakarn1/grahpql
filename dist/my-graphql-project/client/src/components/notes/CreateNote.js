"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = require("react");
const generated_types_1 = require("../../generated-types");
const core_1 = require("@material-ui/core");
require("./Note.css");
const CreateNote = () => {
    const [createNote] = generated_types_1.useCreateNoteMutation();
    const [newNoteTitle, setNewNoteTitle] = react_1.useState('');
    const [newNoteDescription, setNewNoteDescription] = react_1.useState('');
    return (<div>
      <core_1.Card className="inputCard">
        <form noValidate autoComplete="off" className="inputForm">
          <h3>Create Note</h3>
          <p>This application works only with sample Node/Comment model</p>
          <core_1.TextField label="Title" variant="outlined" onChange={(e) => setNewNoteTitle(e.target.value)} value={newNoteTitle}/>
          <core_1.TextField label="Description" variant="outlined" onChange={(e) => setNewNoteDescription(e.target.value)} value={newNoteDescription}/>
          <core_1.Button variant="outlined" color="primary" onClick={() => {
        createNote({ variables: { input: { title: newNoteTitle, description: newNoteDescription } } });
    }}>
            Add Note
          </core_1.Button>
        </form>
      </core_1.Card>
    </div>);
};
exports.default = CreateNote;
//# sourceMappingURL=CreateNote.js.map