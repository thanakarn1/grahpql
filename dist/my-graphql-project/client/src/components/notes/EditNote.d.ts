import './Note.css';
declare type noteProps = {
    id: string;
    title: string;
    description: string | undefined;
    editState: any;
};
declare const EditNote: ({ id, title, description, editState }: noteProps) => any;
export default EditNote;
