"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = require("react");
const EditNote_1 = require("./EditNote");
const CreateComment_1 = require("../comment/CreateComment");
const OneComment_1 = require("../comment/OneComment");
const core_1 = require("@material-ui/core");
const OneNote = ({ id, title, description, comments }) => {
    const [noteEdit, setNoteEdit] = react_1.useState(false);
    const [addComment, setAddComment] = react_1.useState(false);
    return (<div>
      <core_1.Card className="inputCard">
        <li className="OneNote">
          <strong>{title}</strong>:&nbsp;
          {description}
          <core_1.Button onClick={() => setNoteEdit(!noteEdit)} variant="outlined" color="primary">
            Edit
          </core_1.Button>
          <core_1.Button onClick={() => setAddComment(!addComment)} variant="outlined" color="primary">
            Add Comment
          </core_1.Button>
          {noteEdit ? (<EditNote_1.default id={id} title={title} description={description || ''} editState={setNoteEdit}></EditNote_1.default>) : (<div></div>)}
          {addComment ? <CreateComment_1.default noteId={id} addCommentState={setAddComment}></CreateComment_1.default> : <div></div>}
          <ul>
            {comments && comments.length > 0 ? (comments.map((com) => {
        if (!com) {
            return;
        }
        return <OneComment_1.default id={com.id} text={com.text} description={com.description} key={com.id}></OneComment_1.default>;
    })) : (<div></div>)}
          </ul>
        </li>
      </core_1.Card>
    </div>);
};
exports.default = OneNote;
//# sourceMappingURL=OneNote.js.map