import './../notes/Note.css';
declare type createCommentProps = {
    noteId: string;
    addCommentState: any;
};
declare const CreateComment: ({ noteId, addCommentState }: createCommentProps) => any;
export default CreateComment;
