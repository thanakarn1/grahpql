"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = require("react");
const generated_types_1 = require("../../generated-types");
const core_1 = require("@material-ui/core");
require("./Comment.css");
const OneComment = ({ id, text, description }) => {
    const [deleteComment] = generated_types_1.useDeleteCommentMutation();
    return (<div>
      <li className="comment">
        <strong>{text}</strong>:&nbsp;
        {description}
        <core_1.Button variant="outlined" color="secondary" onClick={() => deleteComment({ variables: { input: { id: id } } })}>
          Delete Comment
        </core_1.Button>
      </li>
    </div>);
};
exports.default = OneComment;
//# sourceMappingURL=OneComment.js.map