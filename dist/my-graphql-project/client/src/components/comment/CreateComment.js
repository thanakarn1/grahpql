"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = require("react");
const generated_types_1 = require("../../generated-types");
const core_1 = require("@material-ui/core");
require("./../notes/Note.css");
const CreateComment = ({ noteId, addCommentState }) => {
    const [createComment] = generated_types_1.useCreateCommentMutation();
    const [newCommentTitle, setNewCommentTitle] = react_1.useState('');
    const [newCommentDescription, setNewCommentDescription] = react_1.useState('');
    return (<div>
      <core_1.Card className="inputCard">
        <form noValidate autoComplete="off" className="inputForm">
          <h3>Create Comment</h3>
          <core_1.TextField label="Title" variant="outlined" onChange={(e) => setNewCommentTitle(e.target.value)} value={newCommentTitle}/>
          <core_1.TextField label="Description" variant="outlined" onChange={(e) => setNewCommentDescription(e.target.value)} value={newCommentDescription}/>
          <core_1.Button variant="outlined" color="primary" onClick={() => {
        createComment({
            variables: { input: { text: newCommentTitle, description: newCommentDescription, noteId: noteId } },
        });
        addCommentState(false);
    }}>
            Add Comment
          </core_1.Button>
        </form>
      </core_1.Card>
    </div>);
};
exports.default = CreateComment;
//# sourceMappingURL=CreateComment.js.map