"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.useUpdatedNoteSubscription = exports.UpdatedNoteDocument = exports.useUpdatedCommentSubscription = exports.UpdatedCommentDocument = exports.useNewNoteSubscription = exports.NewNoteDocument = exports.useNewCommentSubscription = exports.NewCommentDocument = exports.useDeletedNoteSubscription = exports.DeletedNoteDocument = exports.useDeletedCommentSubscription = exports.DeletedCommentDocument = exports.useFindNotesLazyQuery = exports.useFindNotesQuery = exports.FindNotesDocument = exports.useFindCommentsLazyQuery = exports.useFindCommentsQuery = exports.FindCommentsDocument = exports.useFindAllNotesLazyQuery = exports.useFindAllNotesQuery = exports.FindAllNotesDocument = exports.useFindAllCommentsLazyQuery = exports.useFindAllCommentsQuery = exports.FindAllCommentsDocument = exports.useUpdateNoteMutation = exports.UpdateNoteDocument = exports.useUpdateCommentMutation = exports.UpdateCommentDocument = exports.useDeleteNoteMutation = exports.DeleteNoteDocument = exports.useDeleteCommentMutation = exports.DeleteCommentDocument = exports.useCreateNoteMutation = exports.CreateNoteDocument = exports.useCreateCommentMutation = exports.CreateCommentDocument = exports.NoteExpandedFieldsFragmentDoc = exports.NoteFieldsFragmentDoc = exports.CommentExpandedFieldsFragmentDoc = exports.CommentFieldsFragmentDoc = void 0;
const graphql_tag_1 = require("graphql-tag");
const ApolloReactHooks = require("@apollo/react-hooks");
exports.CommentFieldsFragmentDoc = graphql_tag_1.default `
    fragment CommentFields on Comment {
  id
  text
  description
}
    `;
exports.CommentExpandedFieldsFragmentDoc = graphql_tag_1.default `
    fragment CommentExpandedFields on Comment {
  id
  text
  description
  note {
    id
    title
    description
  }
}
    `;
exports.NoteFieldsFragmentDoc = graphql_tag_1.default `
    fragment NoteFields on Note {
  id
  title
  description
}
    `;
exports.NoteExpandedFieldsFragmentDoc = graphql_tag_1.default `
    fragment NoteExpandedFields on Note {
  id
  title
  description
  comments {
    id
    text
    description
  }
}
    `;
exports.CreateCommentDocument = graphql_tag_1.default `
    mutation createComment($input: CommentInput!) {
  createComment(input: $input) {
    ...CommentFields
  }
}
    ${exports.CommentFieldsFragmentDoc}`;
function useCreateCommentMutation(baseOptions) {
    return ApolloReactHooks.useMutation(exports.CreateCommentDocument, baseOptions);
}
exports.useCreateCommentMutation = useCreateCommentMutation;
exports.CreateNoteDocument = graphql_tag_1.default `
    mutation createNote($input: NoteInput!) {
  createNote(input: $input) {
    ...NoteFields
  }
}
    ${exports.NoteFieldsFragmentDoc}`;
function useCreateNoteMutation(baseOptions) {
    return ApolloReactHooks.useMutation(exports.CreateNoteDocument, baseOptions);
}
exports.useCreateNoteMutation = useCreateNoteMutation;
exports.DeleteCommentDocument = graphql_tag_1.default `
    mutation deleteComment($input: CommentInput!) {
  deleteComment(input: $input) {
    ...CommentFields
  }
}
    ${exports.CommentFieldsFragmentDoc}`;
function useDeleteCommentMutation(baseOptions) {
    return ApolloReactHooks.useMutation(exports.DeleteCommentDocument, baseOptions);
}
exports.useDeleteCommentMutation = useDeleteCommentMutation;
exports.DeleteNoteDocument = graphql_tag_1.default `
    mutation deleteNote($input: NoteInput!) {
  deleteNote(input: $input) {
    ...NoteFields
  }
}
    ${exports.NoteFieldsFragmentDoc}`;
function useDeleteNoteMutation(baseOptions) {
    return ApolloReactHooks.useMutation(exports.DeleteNoteDocument, baseOptions);
}
exports.useDeleteNoteMutation = useDeleteNoteMutation;
exports.UpdateCommentDocument = graphql_tag_1.default `
    mutation updateComment($input: CommentInput!) {
  updateComment(input: $input) {
    ...CommentFields
  }
}
    ${exports.CommentFieldsFragmentDoc}`;
function useUpdateCommentMutation(baseOptions) {
    return ApolloReactHooks.useMutation(exports.UpdateCommentDocument, baseOptions);
}
exports.useUpdateCommentMutation = useUpdateCommentMutation;
exports.UpdateNoteDocument = graphql_tag_1.default `
    mutation updateNote($input: NoteInput!) {
  updateNote(input: $input) {
    ...NoteFields
  }
}
    ${exports.NoteFieldsFragmentDoc}`;
function useUpdateNoteMutation(baseOptions) {
    return ApolloReactHooks.useMutation(exports.UpdateNoteDocument, baseOptions);
}
exports.useUpdateNoteMutation = useUpdateNoteMutation;
exports.FindAllCommentsDocument = graphql_tag_1.default `
    query findAllComments($limit: Int, $offset: Int) {
  findAllComments(limit: $limit, offset: $offset) {
    ...CommentExpandedFields
  }
}
    ${exports.CommentExpandedFieldsFragmentDoc}`;
function useFindAllCommentsQuery(baseOptions) {
    return ApolloReactHooks.useQuery(exports.FindAllCommentsDocument, baseOptions);
}
exports.useFindAllCommentsQuery = useFindAllCommentsQuery;
function useFindAllCommentsLazyQuery(baseOptions) {
    return ApolloReactHooks.useLazyQuery(exports.FindAllCommentsDocument, baseOptions);
}
exports.useFindAllCommentsLazyQuery = useFindAllCommentsLazyQuery;
exports.FindAllNotesDocument = graphql_tag_1.default `
    query findAllNotes($limit: Int, $offset: Int) {
  findAllNotes(limit: $limit, offset: $offset) {
    ...NoteExpandedFields
  }
}
    ${exports.NoteExpandedFieldsFragmentDoc}`;
function useFindAllNotesQuery(baseOptions) {
    return ApolloReactHooks.useQuery(exports.FindAllNotesDocument, baseOptions);
}
exports.useFindAllNotesQuery = useFindAllNotesQuery;
function useFindAllNotesLazyQuery(baseOptions) {
    return ApolloReactHooks.useLazyQuery(exports.FindAllNotesDocument, baseOptions);
}
exports.useFindAllNotesLazyQuery = useFindAllNotesLazyQuery;
exports.FindCommentsDocument = graphql_tag_1.default `
    query findComments($fields: CommentInput!, $limit: Int, $offset: Int) {
  findComments(fields: $fields, limit: $limit, offset: $offset) {
    ...CommentExpandedFields
  }
}
    ${exports.CommentExpandedFieldsFragmentDoc}`;
function useFindCommentsQuery(baseOptions) {
    return ApolloReactHooks.useQuery(exports.FindCommentsDocument, baseOptions);
}
exports.useFindCommentsQuery = useFindCommentsQuery;
function useFindCommentsLazyQuery(baseOptions) {
    return ApolloReactHooks.useLazyQuery(exports.FindCommentsDocument, baseOptions);
}
exports.useFindCommentsLazyQuery = useFindCommentsLazyQuery;
exports.FindNotesDocument = graphql_tag_1.default `
    query findNotes($fields: NoteInput!, $limit: Int, $offset: Int) {
  findNotes(fields: $fields, limit: $limit, offset: $offset) {
    ...NoteExpandedFields
  }
}
    ${exports.NoteExpandedFieldsFragmentDoc}`;
function useFindNotesQuery(baseOptions) {
    return ApolloReactHooks.useQuery(exports.FindNotesDocument, baseOptions);
}
exports.useFindNotesQuery = useFindNotesQuery;
function useFindNotesLazyQuery(baseOptions) {
    return ApolloReactHooks.useLazyQuery(exports.FindNotesDocument, baseOptions);
}
exports.useFindNotesLazyQuery = useFindNotesLazyQuery;
exports.DeletedCommentDocument = graphql_tag_1.default `
    subscription deletedComment {
  deletedComment {
    ...CommentFields
  }
}
    ${exports.CommentFieldsFragmentDoc}`;
function useDeletedCommentSubscription(baseOptions) {
    return ApolloReactHooks.useSubscription(exports.DeletedCommentDocument, baseOptions);
}
exports.useDeletedCommentSubscription = useDeletedCommentSubscription;
exports.DeletedNoteDocument = graphql_tag_1.default `
    subscription deletedNote {
  deletedNote {
    ...NoteFields
  }
}
    ${exports.NoteFieldsFragmentDoc}`;
function useDeletedNoteSubscription(baseOptions) {
    return ApolloReactHooks.useSubscription(exports.DeletedNoteDocument, baseOptions);
}
exports.useDeletedNoteSubscription = useDeletedNoteSubscription;
exports.NewCommentDocument = graphql_tag_1.default `
    subscription newComment {
  newComment {
    ...CommentFields
  }
}
    ${exports.CommentFieldsFragmentDoc}`;
function useNewCommentSubscription(baseOptions) {
    return ApolloReactHooks.useSubscription(exports.NewCommentDocument, baseOptions);
}
exports.useNewCommentSubscription = useNewCommentSubscription;
exports.NewNoteDocument = graphql_tag_1.default `
    subscription newNote {
  newNote {
    ...NoteFields
  }
}
    ${exports.NoteFieldsFragmentDoc}`;
function useNewNoteSubscription(baseOptions) {
    return ApolloReactHooks.useSubscription(exports.NewNoteDocument, baseOptions);
}
exports.useNewNoteSubscription = useNewNoteSubscription;
exports.UpdatedCommentDocument = graphql_tag_1.default `
    subscription updatedComment {
  updatedComment {
    ...CommentFields
  }
}
    ${exports.CommentFieldsFragmentDoc}`;
function useUpdatedCommentSubscription(baseOptions) {
    return ApolloReactHooks.useSubscription(exports.UpdatedCommentDocument, baseOptions);
}
exports.useUpdatedCommentSubscription = useUpdatedCommentSubscription;
exports.UpdatedNoteDocument = graphql_tag_1.default `
    subscription updatedNote {
  updatedNote {
    ...NoteFields
  }
}
    ${exports.NoteFieldsFragmentDoc}`;
function useUpdatedNoteSubscription(baseOptions) {
    return ApolloReactHooks.useSubscription(exports.UpdatedNoteDocument, baseOptions);
}
exports.useUpdatedNoteSubscription = useUpdatedNoteSubscription;
//# sourceMappingURL=generated-types.js.map