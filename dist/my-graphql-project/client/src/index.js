"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const react_hooks_1 = require("@apollo/react-hooks");
const apollo_boost_1 = require("apollo-boost");
const react_1 = require("react");
const react_dom_1 = require("react-dom");
const App_1 = require("./App");
require("./index.css");
const serviceWorker = require("./serviceWorker");
const apolloClient = new apollo_boost_1.default({
    uri: 'http://localhost:4000/graphql',
});
react_dom_1.default.render(<react_hooks_1.ApolloProvider client={apolloClient}>
    <App_1.default />
  </react_hooks_1.ApolloProvider>, document.getElementById('root'));
serviceWorker.unregister();
//# sourceMappingURL=index.js.map