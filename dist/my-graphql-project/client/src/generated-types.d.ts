import * as ApolloReactCommon from '@apollo/react-common';
import * as ApolloReactHooks from '@apollo/react-hooks';
export declare type Maybe<T> = T | null;
export declare type Scalars = {
    ID: string;
    String: string;
    Boolean: boolean;
    Int: number;
    Float: number;
};
export declare type Comment = {
    __typename?: 'Comment';
    id: Scalars['ID'];
    text?: Maybe<Scalars['String']>;
    description?: Maybe<Scalars['String']>;
    note?: Maybe<Note>;
};
export declare type CommentInput = {
    id?: Maybe<Scalars['ID']>;
    text?: Maybe<Scalars['String']>;
    description?: Maybe<Scalars['String']>;
    noteId?: Maybe<Scalars['ID']>;
};
export declare type Mutation = {
    __typename?: 'Mutation';
    createNote: Note;
    updateNote: Note;
    deleteNote: Note;
    createComment: Comment;
    updateComment: Comment;
    deleteComment: Comment;
};
export declare type MutationCreateNoteArgs = {
    input?: Maybe<NoteInput>;
};
export declare type MutationUpdateNoteArgs = {
    input?: Maybe<NoteInput>;
};
export declare type MutationDeleteNoteArgs = {
    input?: Maybe<NoteInput>;
};
export declare type MutationCreateCommentArgs = {
    input?: Maybe<CommentInput>;
};
export declare type MutationUpdateCommentArgs = {
    input?: Maybe<CommentInput>;
};
export declare type MutationDeleteCommentArgs = {
    input?: Maybe<CommentInput>;
};
export declare type Note = {
    __typename?: 'Note';
    id: Scalars['ID'];
    title: Scalars['String'];
    description?: Maybe<Scalars['String']>;
    comments: Array<Maybe<Comment>>;
};
export declare type NoteInput = {
    id?: Maybe<Scalars['ID']>;
    title?: Maybe<Scalars['String']>;
    description?: Maybe<Scalars['String']>;
};
export declare type Query = {
    __typename?: 'Query';
    findAllNotes: Array<Maybe<Note>>;
    findNotes: Array<Maybe<Note>>;
    findAllComments: Array<Maybe<Comment>>;
    findComments: Array<Maybe<Comment>>;
};
export declare type QueryFindAllNotesArgs = {
    limit?: Maybe<Scalars['Int']>;
    offset?: Maybe<Scalars['Int']>;
};
export declare type QueryFindNotesArgs = {
    fields?: Maybe<NoteInput>;
    limit?: Maybe<Scalars['Int']>;
    offset?: Maybe<Scalars['Int']>;
};
export declare type QueryFindAllCommentsArgs = {
    limit?: Maybe<Scalars['Int']>;
    offset?: Maybe<Scalars['Int']>;
};
export declare type QueryFindCommentsArgs = {
    fields?: Maybe<CommentInput>;
    limit?: Maybe<Scalars['Int']>;
    offset?: Maybe<Scalars['Int']>;
};
export declare type Subscription = {
    __typename?: 'Subscription';
    newNote: Note;
    updatedNote: Note;
    deletedNote: Note;
    newComment: Comment;
    updatedComment: Comment;
    deletedComment: Comment;
};
export declare type SubscriptionNewNoteArgs = {
    input?: Maybe<NoteInput>;
};
export declare type SubscriptionUpdatedNoteArgs = {
    input?: Maybe<NoteInput>;
};
export declare type SubscriptionDeletedNoteArgs = {
    input?: Maybe<NoteInput>;
};
export declare type SubscriptionNewCommentArgs = {
    input?: Maybe<CommentInput>;
};
export declare type SubscriptionUpdatedCommentArgs = {
    input?: Maybe<CommentInput>;
};
export declare type SubscriptionDeletedCommentArgs = {
    input?: Maybe<CommentInput>;
};
export declare type CommentFieldsFragment = ({
    __typename?: 'Comment';
} & Pick<Comment, 'id' | 'text' | 'description'>);
export declare type CommentExpandedFieldsFragment = ({
    __typename?: 'Comment';
} & Pick<Comment, 'id' | 'text' | 'description'> & {
    note?: Maybe<({
        __typename?: 'Note';
    } & Pick<Note, 'id' | 'title' | 'description'>)>;
});
export declare type NoteFieldsFragment = ({
    __typename?: 'Note';
} & Pick<Note, 'id' | 'title' | 'description'>);
export declare type NoteExpandedFieldsFragment = ({
    __typename?: 'Note';
} & Pick<Note, 'id' | 'title' | 'description'> & {
    comments: Array<Maybe<({
        __typename?: 'Comment';
    } & Pick<Comment, 'id' | 'text' | 'description'>)>>;
});
export declare type CreateCommentMutationVariables = {
    input: CommentInput;
};
export declare type CreateCommentMutation = ({
    __typename?: 'Mutation';
} & {
    createComment: ({
        __typename?: 'Comment';
    } & CommentFieldsFragment);
});
export declare type CreateNoteMutationVariables = {
    input: NoteInput;
};
export declare type CreateNoteMutation = ({
    __typename?: 'Mutation';
} & {
    createNote: ({
        __typename?: 'Note';
    } & NoteFieldsFragment);
});
export declare type DeleteCommentMutationVariables = {
    input: CommentInput;
};
export declare type DeleteCommentMutation = ({
    __typename?: 'Mutation';
} & {
    deleteComment: ({
        __typename?: 'Comment';
    } & CommentFieldsFragment);
});
export declare type DeleteNoteMutationVariables = {
    input: NoteInput;
};
export declare type DeleteNoteMutation = ({
    __typename?: 'Mutation';
} & {
    deleteNote: ({
        __typename?: 'Note';
    } & NoteFieldsFragment);
});
export declare type UpdateCommentMutationVariables = {
    input: CommentInput;
};
export declare type UpdateCommentMutation = ({
    __typename?: 'Mutation';
} & {
    updateComment: ({
        __typename?: 'Comment';
    } & CommentFieldsFragment);
});
export declare type UpdateNoteMutationVariables = {
    input: NoteInput;
};
export declare type UpdateNoteMutation = ({
    __typename?: 'Mutation';
} & {
    updateNote: ({
        __typename?: 'Note';
    } & NoteFieldsFragment);
});
export declare type FindAllCommentsQueryVariables = {
    limit?: Maybe<Scalars['Int']>;
    offset?: Maybe<Scalars['Int']>;
};
export declare type FindAllCommentsQuery = ({
    __typename?: 'Query';
} & {
    findAllComments: Array<Maybe<({
        __typename?: 'Comment';
    } & CommentExpandedFieldsFragment)>>;
});
export declare type FindAllNotesQueryVariables = {
    limit?: Maybe<Scalars['Int']>;
    offset?: Maybe<Scalars['Int']>;
};
export declare type FindAllNotesQuery = ({
    __typename?: 'Query';
} & {
    findAllNotes: Array<Maybe<({
        __typename?: 'Note';
    } & NoteExpandedFieldsFragment)>>;
});
export declare type FindCommentsQueryVariables = {
    fields: CommentInput;
    limit?: Maybe<Scalars['Int']>;
    offset?: Maybe<Scalars['Int']>;
};
export declare type FindCommentsQuery = ({
    __typename?: 'Query';
} & {
    findComments: Array<Maybe<({
        __typename?: 'Comment';
    } & CommentExpandedFieldsFragment)>>;
});
export declare type FindNotesQueryVariables = {
    fields: NoteInput;
    limit?: Maybe<Scalars['Int']>;
    offset?: Maybe<Scalars['Int']>;
};
export declare type FindNotesQuery = ({
    __typename?: 'Query';
} & {
    findNotes: Array<Maybe<({
        __typename?: 'Note';
    } & NoteExpandedFieldsFragment)>>;
});
export declare type DeletedCommentSubscriptionVariables = {};
export declare type DeletedCommentSubscription = ({
    __typename?: 'Subscription';
} & {
    deletedComment: ({
        __typename?: 'Comment';
    } & CommentFieldsFragment);
});
export declare type DeletedNoteSubscriptionVariables = {};
export declare type DeletedNoteSubscription = ({
    __typename?: 'Subscription';
} & {
    deletedNote: ({
        __typename?: 'Note';
    } & NoteFieldsFragment);
});
export declare type NewCommentSubscriptionVariables = {};
export declare type NewCommentSubscription = ({
    __typename?: 'Subscription';
} & {
    newComment: ({
        __typename?: 'Comment';
    } & CommentFieldsFragment);
});
export declare type NewNoteSubscriptionVariables = {};
export declare type NewNoteSubscription = ({
    __typename?: 'Subscription';
} & {
    newNote: ({
        __typename?: 'Note';
    } & NoteFieldsFragment);
});
export declare type UpdatedCommentSubscriptionVariables = {};
export declare type UpdatedCommentSubscription = ({
    __typename?: 'Subscription';
} & {
    updatedComment: ({
        __typename?: 'Comment';
    } & CommentFieldsFragment);
});
export declare type UpdatedNoteSubscriptionVariables = {};
export declare type UpdatedNoteSubscription = ({
    __typename?: 'Subscription';
} & {
    updatedNote: ({
        __typename?: 'Note';
    } & NoteFieldsFragment);
});
export declare const CommentFieldsFragmentDoc: import("graphql").DocumentNode;
export declare const CommentExpandedFieldsFragmentDoc: import("graphql").DocumentNode;
export declare const NoteFieldsFragmentDoc: import("graphql").DocumentNode;
export declare const NoteExpandedFieldsFragmentDoc: import("graphql").DocumentNode;
export declare const CreateCommentDocument: import("graphql").DocumentNode;
export declare type CreateCommentMutationFn = ApolloReactCommon.MutationFunction<CreateCommentMutation, CreateCommentMutationVariables>;
export declare function useCreateCommentMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<CreateCommentMutation, CreateCommentMutationVariables>): any;
export declare type CreateCommentMutationHookResult = ReturnType<typeof useCreateCommentMutation>;
export declare type CreateCommentMutationResult = ApolloReactCommon.MutationResult<CreateCommentMutation>;
export declare type CreateCommentMutationOptions = ApolloReactCommon.BaseMutationOptions<CreateCommentMutation, CreateCommentMutationVariables>;
export declare const CreateNoteDocument: import("graphql").DocumentNode;
export declare type CreateNoteMutationFn = ApolloReactCommon.MutationFunction<CreateNoteMutation, CreateNoteMutationVariables>;
export declare function useCreateNoteMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<CreateNoteMutation, CreateNoteMutationVariables>): any;
export declare type CreateNoteMutationHookResult = ReturnType<typeof useCreateNoteMutation>;
export declare type CreateNoteMutationResult = ApolloReactCommon.MutationResult<CreateNoteMutation>;
export declare type CreateNoteMutationOptions = ApolloReactCommon.BaseMutationOptions<CreateNoteMutation, CreateNoteMutationVariables>;
export declare const DeleteCommentDocument: import("graphql").DocumentNode;
export declare type DeleteCommentMutationFn = ApolloReactCommon.MutationFunction<DeleteCommentMutation, DeleteCommentMutationVariables>;
export declare function useDeleteCommentMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<DeleteCommentMutation, DeleteCommentMutationVariables>): any;
export declare type DeleteCommentMutationHookResult = ReturnType<typeof useDeleteCommentMutation>;
export declare type DeleteCommentMutationResult = ApolloReactCommon.MutationResult<DeleteCommentMutation>;
export declare type DeleteCommentMutationOptions = ApolloReactCommon.BaseMutationOptions<DeleteCommentMutation, DeleteCommentMutationVariables>;
export declare const DeleteNoteDocument: import("graphql").DocumentNode;
export declare type DeleteNoteMutationFn = ApolloReactCommon.MutationFunction<DeleteNoteMutation, DeleteNoteMutationVariables>;
export declare function useDeleteNoteMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<DeleteNoteMutation, DeleteNoteMutationVariables>): any;
export declare type DeleteNoteMutationHookResult = ReturnType<typeof useDeleteNoteMutation>;
export declare type DeleteNoteMutationResult = ApolloReactCommon.MutationResult<DeleteNoteMutation>;
export declare type DeleteNoteMutationOptions = ApolloReactCommon.BaseMutationOptions<DeleteNoteMutation, DeleteNoteMutationVariables>;
export declare const UpdateCommentDocument: import("graphql").DocumentNode;
export declare type UpdateCommentMutationFn = ApolloReactCommon.MutationFunction<UpdateCommentMutation, UpdateCommentMutationVariables>;
export declare function useUpdateCommentMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<UpdateCommentMutation, UpdateCommentMutationVariables>): any;
export declare type UpdateCommentMutationHookResult = ReturnType<typeof useUpdateCommentMutation>;
export declare type UpdateCommentMutationResult = ApolloReactCommon.MutationResult<UpdateCommentMutation>;
export declare type UpdateCommentMutationOptions = ApolloReactCommon.BaseMutationOptions<UpdateCommentMutation, UpdateCommentMutationVariables>;
export declare const UpdateNoteDocument: import("graphql").DocumentNode;
export declare type UpdateNoteMutationFn = ApolloReactCommon.MutationFunction<UpdateNoteMutation, UpdateNoteMutationVariables>;
export declare function useUpdateNoteMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<UpdateNoteMutation, UpdateNoteMutationVariables>): any;
export declare type UpdateNoteMutationHookResult = ReturnType<typeof useUpdateNoteMutation>;
export declare type UpdateNoteMutationResult = ApolloReactCommon.MutationResult<UpdateNoteMutation>;
export declare type UpdateNoteMutationOptions = ApolloReactCommon.BaseMutationOptions<UpdateNoteMutation, UpdateNoteMutationVariables>;
export declare const FindAllCommentsDocument: import("graphql").DocumentNode;
export declare function useFindAllCommentsQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<FindAllCommentsQuery, FindAllCommentsQueryVariables>): any;
export declare function useFindAllCommentsLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<FindAllCommentsQuery, FindAllCommentsQueryVariables>): any;
export declare type FindAllCommentsQueryHookResult = ReturnType<typeof useFindAllCommentsQuery>;
export declare type FindAllCommentsLazyQueryHookResult = ReturnType<typeof useFindAllCommentsLazyQuery>;
export declare type FindAllCommentsQueryResult = ApolloReactCommon.QueryResult<FindAllCommentsQuery, FindAllCommentsQueryVariables>;
export declare const FindAllNotesDocument: import("graphql").DocumentNode;
export declare function useFindAllNotesQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<FindAllNotesQuery, FindAllNotesQueryVariables>): any;
export declare function useFindAllNotesLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<FindAllNotesQuery, FindAllNotesQueryVariables>): any;
export declare type FindAllNotesQueryHookResult = ReturnType<typeof useFindAllNotesQuery>;
export declare type FindAllNotesLazyQueryHookResult = ReturnType<typeof useFindAllNotesLazyQuery>;
export declare type FindAllNotesQueryResult = ApolloReactCommon.QueryResult<FindAllNotesQuery, FindAllNotesQueryVariables>;
export declare const FindCommentsDocument: import("graphql").DocumentNode;
export declare function useFindCommentsQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<FindCommentsQuery, FindCommentsQueryVariables>): any;
export declare function useFindCommentsLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<FindCommentsQuery, FindCommentsQueryVariables>): any;
export declare type FindCommentsQueryHookResult = ReturnType<typeof useFindCommentsQuery>;
export declare type FindCommentsLazyQueryHookResult = ReturnType<typeof useFindCommentsLazyQuery>;
export declare type FindCommentsQueryResult = ApolloReactCommon.QueryResult<FindCommentsQuery, FindCommentsQueryVariables>;
export declare const FindNotesDocument: import("graphql").DocumentNode;
export declare function useFindNotesQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<FindNotesQuery, FindNotesQueryVariables>): any;
export declare function useFindNotesLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<FindNotesQuery, FindNotesQueryVariables>): any;
export declare type FindNotesQueryHookResult = ReturnType<typeof useFindNotesQuery>;
export declare type FindNotesLazyQueryHookResult = ReturnType<typeof useFindNotesLazyQuery>;
export declare type FindNotesQueryResult = ApolloReactCommon.QueryResult<FindNotesQuery, FindNotesQueryVariables>;
export declare const DeletedCommentDocument: import("graphql").DocumentNode;
export declare function useDeletedCommentSubscription(baseOptions?: ApolloReactHooks.SubscriptionHookOptions<DeletedCommentSubscription, DeletedCommentSubscriptionVariables>): any;
export declare type DeletedCommentSubscriptionHookResult = ReturnType<typeof useDeletedCommentSubscription>;
export declare type DeletedCommentSubscriptionResult = ApolloReactCommon.SubscriptionResult<DeletedCommentSubscription>;
export declare const DeletedNoteDocument: import("graphql").DocumentNode;
export declare function useDeletedNoteSubscription(baseOptions?: ApolloReactHooks.SubscriptionHookOptions<DeletedNoteSubscription, DeletedNoteSubscriptionVariables>): any;
export declare type DeletedNoteSubscriptionHookResult = ReturnType<typeof useDeletedNoteSubscription>;
export declare type DeletedNoteSubscriptionResult = ApolloReactCommon.SubscriptionResult<DeletedNoteSubscription>;
export declare const NewCommentDocument: import("graphql").DocumentNode;
export declare function useNewCommentSubscription(baseOptions?: ApolloReactHooks.SubscriptionHookOptions<NewCommentSubscription, NewCommentSubscriptionVariables>): any;
export declare type NewCommentSubscriptionHookResult = ReturnType<typeof useNewCommentSubscription>;
export declare type NewCommentSubscriptionResult = ApolloReactCommon.SubscriptionResult<NewCommentSubscription>;
export declare const NewNoteDocument: import("graphql").DocumentNode;
export declare function useNewNoteSubscription(baseOptions?: ApolloReactHooks.SubscriptionHookOptions<NewNoteSubscription, NewNoteSubscriptionVariables>): any;
export declare type NewNoteSubscriptionHookResult = ReturnType<typeof useNewNoteSubscription>;
export declare type NewNoteSubscriptionResult = ApolloReactCommon.SubscriptionResult<NewNoteSubscription>;
export declare const UpdatedCommentDocument: import("graphql").DocumentNode;
export declare function useUpdatedCommentSubscription(baseOptions?: ApolloReactHooks.SubscriptionHookOptions<UpdatedCommentSubscription, UpdatedCommentSubscriptionVariables>): any;
export declare type UpdatedCommentSubscriptionHookResult = ReturnType<typeof useUpdatedCommentSubscription>;
export declare type UpdatedCommentSubscriptionResult = ApolloReactCommon.SubscriptionResult<UpdatedCommentSubscription>;
export declare const UpdatedNoteDocument: import("graphql").DocumentNode;
export declare function useUpdatedNoteSubscription(baseOptions?: ApolloReactHooks.SubscriptionHookOptions<UpdatedNoteSubscription, UpdatedNoteSubscriptionVariables>): any;
export declare type UpdatedNoteSubscriptionHookResult = ReturnType<typeof useUpdatedNoteSubscription>;
export declare type UpdatedNoteSubscriptionResult = ApolloReactCommon.SubscriptionResult<UpdatedNoteSubscription>;
