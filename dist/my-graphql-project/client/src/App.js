"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = require("react");
require("./App.css");
const generated_types_1 = require("./generated-types");
const CreateNote_1 = require("./components/notes/CreateNote");
const OneNote_1 = require("./components/notes/OneNote");
const App = () => {
    var _a;
    const allNotes = generated_types_1.useFindAllNotesQuery();
    allNotes.startPolling(2000);
    console.log((_a = allNotes.data) === null || _a === void 0 ? void 0 : _a.findAllNotes);
    return (<div>
      <CreateNote_1.default></CreateNote_1.default>
      <ul>
        {allNotes.data &&
        allNotes.data.findAllNotes.map((note) => (<OneNote_1.default key={note.id} id={note.id} title={note.title} description={note.description} comments={note.comments}></OneNote_1.default>))}
      </ul>
    </div>);
};
exports.default = App;
//# sourceMappingURL=App.js.map