import { CustomerService } from './customer.service';
import { CustomerModel } from './customer.model';
export declare class CustomerResolver {
    private customerService;
    constructor(customerService: CustomerService);
    customer(id: string): Promise<CustomerModel>;
    customers(): Promise<CustomerModel[]>;
    createCustomer(name: string, email: string, phone: string, address: string): Promise<CustomerModel>;
}
