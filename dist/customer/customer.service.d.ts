import { CustomerModel } from './customer.model';
import { Repository } from 'typeorm';
import { CustomerDTO } from './customer.dto';
export declare class CustomerService {
    private customerRepository;
    constructor(customerRepository: Repository<CustomerModel>);
    create(details: CustomerDTO): Promise<CustomerModel>;
    findAll(): Promise<CustomerModel[]>;
    findOne(id: string): Promise<CustomerModel>;
}
