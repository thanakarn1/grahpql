"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomerResolver = void 0;
const customer_service_1 = require("./customer.service");
const customer_model_1 = require("./customer.model");
const graphql_1 = require("@nestjs/graphql");
const common_1 = require("@nestjs/common");
let CustomerResolver = class CustomerResolver {
    constructor(customerService) {
        this.customerService = customerService;
    }
    async customer(id) {
        return await this.customerService.findOne(id);
    }
    async customers() {
        return await this.customerService.findAll();
    }
    async createCustomer(name, email, phone, address) {
        return await this.customerService.create({ name, email, phone, address });
    }
};
__decorate([
    graphql_1.Query(returns => customer_model_1.CustomerModel),
    __param(0, graphql_1.Args('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], CustomerResolver.prototype, "customer", null);
__decorate([
    graphql_1.Query(returns => [customer_model_1.CustomerModel]),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], CustomerResolver.prototype, "customers", null);
__decorate([
    graphql_1.Mutation(returns => customer_model_1.CustomerModel),
    __param(0, graphql_1.Args('name')),
    __param(1, graphql_1.Args('email')),
    __param(2, graphql_1.Args('phone', { nullable: true })),
    __param(3, graphql_1.Args('address', { nullable: true })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, String, String]),
    __metadata("design:returntype", Promise)
], CustomerResolver.prototype, "createCustomer", null);
CustomerResolver = __decorate([
    graphql_1.Resolver(of => customer_model_1.CustomerModel),
    __param(0, common_1.Inject(customer_service_1.CustomerService)),
    __metadata("design:paramtypes", [customer_service_1.CustomerService])
], CustomerResolver);
exports.CustomerResolver = CustomerResolver;
//# sourceMappingURL=customer.resolver.js.map