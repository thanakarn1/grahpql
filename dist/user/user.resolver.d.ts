import { UserService } from './user.service';
import { User } from './user.entity';
export declare class UserResolver {
    private userService;
    constructor(userService: UserService);
    user(id: string): Promise<User>;
    users(): Promise<User[]>;
    createUser(name: string, email: string, phone: string, address: string): Promise<User>;
    delete(id: number): Promise<any>;
    updateUser(id: string, name: string, email: string, phone: string, address: string): Promise<User>;
}
