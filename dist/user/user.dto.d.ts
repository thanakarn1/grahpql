export interface UserDto {
    name: string;
    email: string;
    phone: string;
    address: string;
}
