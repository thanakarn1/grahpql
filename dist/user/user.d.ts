export declare class User {
    id: string;
    name: string;
    email: string;
    phone: string;
    address: string;
    created_at: Date;
    updated_at: Date;
}
