import 'reflect-metadata';
export declare class ErrorResponse {
    path: string;
    message: string;
}
