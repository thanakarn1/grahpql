import { User } from '../user.entity';
export declare class SignupInput implements Partial<User> {
    readonly userName: string;
    readonly email: string;
    readonly password: string;
}
