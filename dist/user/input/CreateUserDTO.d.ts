export declare class CreateUserDTO {
    readonly id?: number;
    readonly userName: string;
    readonly email: string;
    readonly password: string;
}
