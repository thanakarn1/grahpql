"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserResolver = void 0;
const graphql_1 = require("@nestjs/graphql");
const user_service_1 = require("./user.service");
const common_1 = require("@nestjs/common");
const user_entity_1 = require("./user.entity");
let UserResolver = class UserResolver {
    constructor(userService) {
        this.userService = userService;
    }
    async user(id) {
        return await this.userService.findOne(id);
    }
    async users() {
        return await this.userService.findAll();
    }
    async createUser(name, email, phone, address) {
        return await this.userService.create({ name, email, phone, address });
    }
    async delete(id) {
        try {
            await this.userService.delete(id);
            return 'Delete Successful';
        }
        catch (error) {
            return error;
        }
    }
    async updateUser(id, name, email, phone, address) {
        this.userService.update(id, { name, email, phone, address });
        return await this.userService.findOne(id);
    }
};
__decorate([
    graphql_1.Query(returns => user_entity_1.User),
    __param(0, graphql_1.Args('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], UserResolver.prototype, "user", null);
__decorate([
    graphql_1.Query(returns => [user_entity_1.User]),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], UserResolver.prototype, "users", null);
__decorate([
    graphql_1.Mutation(returns => user_entity_1.User),
    __param(0, graphql_1.Args('name')),
    __param(1, graphql_1.Args('email')),
    __param(2, graphql_1.Args('phone', { nullable: true })),
    __param(3, graphql_1.Args('address', { nullable: true })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, String, String]),
    __metadata("design:returntype", Promise)
], UserResolver.prototype, "createUser", null);
__decorate([
    graphql_1.Mutation(() => String),
    __param(0, graphql_1.Args('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], UserResolver.prototype, "delete", null);
__decorate([
    graphql_1.Mutation(returns => user_entity_1.User),
    __param(0, graphql_1.Args('id')),
    __param(1, graphql_1.Args('name')),
    __param(2, graphql_1.Args('email')),
    __param(3, graphql_1.Args('phone', { nullable: true })),
    __param(4, graphql_1.Args('address', { nullable: true })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String, String, String, String]),
    __metadata("design:returntype", Promise)
], UserResolver.prototype, "updateUser", null);
UserResolver = __decorate([
    graphql_1.Resolver('User'),
    __param(0, common_1.Inject(user_service_1.UserService)),
    __metadata("design:paramtypes", [user_service_1.UserService])
], UserResolver);
exports.UserResolver = UserResolver;
//# sourceMappingURL=user.resolver.js.map