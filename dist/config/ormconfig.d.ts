export declare const ormConfig: {
    type: string;
    host: string;
    port: string | number;
    username: string;
    password: string;
    database: string;
    synchronize: boolean;
    logging: boolean;
    logger: string;
    entities: string[];
    migrations: string[];
    subscribers: string[];
    cli: {
        migrationsDir: string;
    };
};
