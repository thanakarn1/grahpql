"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.InvoiceResolver = void 0;
const customer_model_1 = require("./../customer/customer.model");
const customer_service_1 = require("./../customer/customer.service");
const invoice_service_1 = require("./invoice.service");
const graphql_1 = require("@nestjs/graphql");
const common_1 = require("@nestjs/common");
const invoice_model_1 = require("./invoice.model");
const invoice_dto_1 = require("./invoice.dto");
let InvoiceResolver = class InvoiceResolver {
    constructor(invoiceService, customerService) {
        this.invoiceService = invoiceService;
        this.customerService = customerService;
    }
    async invoice(id) {
        return await this.invoiceService.findOne(id);
    }
    async customer(invoice) {
        const { customer } = invoice;
        return this.customerService.findOne(customer);
    }
    async invoices() {
        return await this.invoiceService.findAll();
    }
    async createInvoice(invoice) {
        return await this.invoiceService.create(invoice);
    }
};
__decorate([
    graphql_1.Query(returns => invoice_model_1.InvoiceModel),
    __param(0, graphql_1.Args('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], InvoiceResolver.prototype, "invoice", null);
__decorate([
    graphql_1.ResolveField(returns => customer_model_1.CustomerModel),
    __param(0, graphql_1.Parent()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], InvoiceResolver.prototype, "customer", null);
__decorate([
    graphql_1.Query(returns => [invoice_model_1.InvoiceModel]),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], InvoiceResolver.prototype, "invoices", null);
__decorate([
    graphql_1.Mutation(returns => invoice_model_1.InvoiceModel),
    __param(0, graphql_1.Args('invoice')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [invoice_dto_1.CreateInvoiceDTO]),
    __metadata("design:returntype", Promise)
], InvoiceResolver.prototype, "createInvoice", null);
InvoiceResolver = __decorate([
    graphql_1.Resolver(of => invoice_model_1.InvoiceModel),
    __param(0, common_1.Inject(invoice_service_1.InvoiceService)),
    __param(1, common_1.Inject(customer_service_1.CustomerService)),
    __metadata("design:paramtypes", [invoice_service_1.InvoiceService,
        customer_service_1.CustomerService])
], InvoiceResolver);
exports.InvoiceResolver = InvoiceResolver;
//# sourceMappingURL=invoice.resolver.js.map