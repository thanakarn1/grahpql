import { CustomerModel } from './../customer/customer.model';
import { CustomerService } from './../customer/customer.service';
import { InvoiceService } from './invoice.service';
import { InvoiceModel } from './invoice.model';
import { CreateInvoiceDTO } from './invoice.dto';
export declare class InvoiceResolver {
    private invoiceService;
    private customerService;
    constructor(invoiceService: InvoiceService, customerService: CustomerService);
    invoice(id: string): Promise<InvoiceModel>;
    customer(invoice: any): Promise<CustomerModel>;
    invoices(): Promise<InvoiceModel[]>;
    createInvoice(invoice: CreateInvoiceDTO): Promise<InvoiceModel>;
}
