import { CustomerModel } from './../customer/customer.model';
export declare enum Currency {
    NGN = "NGN",
    USD = "USD",
    GBP = "GBP",
    EUR = " EUR"
}
export declare enum PaymentStatus {
    PAID = "PAID",
    NOT_PAID = "NOT_PAID"
}
export declare class Item {
    description: string;
    rate: number;
    quantity: number;
}
export declare class InvoiceModel {
    id: string;
    invoiceNo: string;
    description: string;
    customer: CustomerModel;
    paymentStatus: PaymentStatus;
    currency: Currency;
    taxRate: number;
    issueDate: string;
    dueDate: string;
    note: string;
    items: Item[];
    taxAmount: number;
    subTotal: number;
    total: string;
    amountPaid: number;
    outstandingBalance: number;
    createdAt: Date;
    updatedAt: Date;
}
