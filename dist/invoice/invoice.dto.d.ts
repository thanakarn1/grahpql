import { PaymentStatus, Currency } from "./invoice.model";
export declare class CreateInvoiceDTO {
    customer: string;
    invoiceNo: string;
    paymentStatus: PaymentStatus;
    description: string;
    currency: Currency;
    taxRate: number;
    issueDate: Date;
    dueDate: Date;
    note: string;
    items: Array<{
        description: string;
        rate: number;
        quantity: number;
    }>;
}
