"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateInvoiceDTO = void 0;
const invoice_model_1 = require("./invoice.model");
const graphql_1 = require("@nestjs/graphql");
let ItemDTO = class ItemDTO {
};
__decorate([
    graphql_1.Field(),
    __metadata("design:type", String)
], ItemDTO.prototype, "description", void 0);
__decorate([
    graphql_1.Field(),
    __metadata("design:type", Number)
], ItemDTO.prototype, "rate", void 0);
__decorate([
    graphql_1.Field(),
    __metadata("design:type", Number)
], ItemDTO.prototype, "quantity", void 0);
ItemDTO = __decorate([
    graphql_1.InputType()
], ItemDTO);
let CreateInvoiceDTO = class CreateInvoiceDTO {
};
__decorate([
    graphql_1.Field(),
    __metadata("design:type", String)
], CreateInvoiceDTO.prototype, "customer", void 0);
__decorate([
    graphql_1.Field(),
    __metadata("design:type", String)
], CreateInvoiceDTO.prototype, "invoiceNo", void 0);
__decorate([
    graphql_1.Field(),
    __metadata("design:type", String)
], CreateInvoiceDTO.prototype, "paymentStatus", void 0);
__decorate([
    graphql_1.Field(),
    __metadata("design:type", String)
], CreateInvoiceDTO.prototype, "description", void 0);
__decorate([
    graphql_1.Field(),
    __metadata("design:type", String)
], CreateInvoiceDTO.prototype, "currency", void 0);
__decorate([
    graphql_1.Field(),
    __metadata("design:type", Number)
], CreateInvoiceDTO.prototype, "taxRate", void 0);
__decorate([
    graphql_1.Field(),
    __metadata("design:type", Date)
], CreateInvoiceDTO.prototype, "issueDate", void 0);
__decorate([
    graphql_1.Field(),
    __metadata("design:type", Date)
], CreateInvoiceDTO.prototype, "dueDate", void 0);
__decorate([
    graphql_1.Field(),
    __metadata("design:type", String)
], CreateInvoiceDTO.prototype, "note", void 0);
__decorate([
    graphql_1.Field(type => [ItemDTO]),
    __metadata("design:type", Array)
], CreateInvoiceDTO.prototype, "items", void 0);
CreateInvoiceDTO = __decorate([
    graphql_1.InputType()
], CreateInvoiceDTO);
exports.CreateInvoiceDTO = CreateInvoiceDTO;
//# sourceMappingURL=invoice.dto.js.map