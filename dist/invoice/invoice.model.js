"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.InvoiceModel = exports.Item = exports.PaymentStatus = exports.Currency = void 0;
const customer_model_1 = require("./../customer/customer.model");
const typeorm_1 = require("typeorm");
const graphql_1 = require("@nestjs/graphql");
var Currency;
(function (Currency) {
    Currency["NGN"] = "NGN";
    Currency["USD"] = "USD";
    Currency["GBP"] = "GBP";
    Currency["EUR"] = " EUR";
})(Currency = exports.Currency || (exports.Currency = {}));
var PaymentStatus;
(function (PaymentStatus) {
    PaymentStatus["PAID"] = "PAID";
    PaymentStatus["NOT_PAID"] = "NOT_PAID";
})(PaymentStatus = exports.PaymentStatus || (exports.PaymentStatus = {}));
let Item = class Item {
};
__decorate([
    graphql_1.Field(),
    __metadata("design:type", String)
], Item.prototype, "description", void 0);
__decorate([
    graphql_1.Field(),
    __metadata("design:type", Number)
], Item.prototype, "rate", void 0);
__decorate([
    graphql_1.Field(),
    __metadata("design:type", Number)
], Item.prototype, "quantity", void 0);
Item = __decorate([
    graphql_1.ObjectType()
], Item);
exports.Item = Item;
let InvoiceModel = class InvoiceModel {
};
__decorate([
    graphql_1.Field(),
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], InvoiceModel.prototype, "id", void 0);
__decorate([
    graphql_1.Field(),
    typeorm_1.Column({ length: 500, nullable: false }),
    __metadata("design:type", String)
], InvoiceModel.prototype, "invoiceNo", void 0);
__decorate([
    graphql_1.Field(),
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], InvoiceModel.prototype, "description", void 0);
__decorate([
    graphql_1.Field(type => customer_model_1.CustomerModel),
    typeorm_1.ManyToOne(type => customer_model_1.CustomerModel, customer => customer.invoices),
    __metadata("design:type", customer_model_1.CustomerModel)
], InvoiceModel.prototype, "customer", void 0);
__decorate([
    graphql_1.Field(),
    typeorm_1.Column({
        type: "enum",
        enum: PaymentStatus,
        default: PaymentStatus.NOT_PAID
    }),
    __metadata("design:type", String)
], InvoiceModel.prototype, "paymentStatus", void 0);
__decorate([
    graphql_1.Field(),
    typeorm_1.Column({
        type: "enum",
        enum: Currency,
        default: Currency.USD
    }),
    __metadata("design:type", String)
], InvoiceModel.prototype, "currency", void 0);
__decorate([
    graphql_1.Field(),
    typeorm_1.Column(),
    __metadata("design:type", Number)
], InvoiceModel.prototype, "taxRate", void 0);
__decorate([
    graphql_1.Field(),
    typeorm_1.Column(),
    __metadata("design:type", String)
], InvoiceModel.prototype, "issueDate", void 0);
__decorate([
    graphql_1.Field(),
    typeorm_1.Column(),
    __metadata("design:type", String)
], InvoiceModel.prototype, "dueDate", void 0);
__decorate([
    graphql_1.Field(),
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], InvoiceModel.prototype, "note", void 0);
__decorate([
    graphql_1.Field(type => [Item]),
    typeorm_1.Column({
        type: 'jsonb',
        array: false,
        default: [],
        nullable: false,
    }),
    __metadata("design:type", Array)
], InvoiceModel.prototype, "items", void 0);
__decorate([
    typeorm_1.Column(),
    graphql_1.Field(),
    __metadata("design:type", Number)
], InvoiceModel.prototype, "taxAmount", void 0);
__decorate([
    typeorm_1.Column(),
    graphql_1.Field(),
    __metadata("design:type", Number)
], InvoiceModel.prototype, "subTotal", void 0);
__decorate([
    typeorm_1.Column(),
    graphql_1.Field(),
    __metadata("design:type", String)
], InvoiceModel.prototype, "total", void 0);
__decorate([
    typeorm_1.Column({
        default: 0
    }),
    graphql_1.Field(),
    __metadata("design:type", Number)
], InvoiceModel.prototype, "amountPaid", void 0);
__decorate([
    typeorm_1.Column(),
    graphql_1.Field(),
    __metadata("design:type", Number)
], InvoiceModel.prototype, "outstandingBalance", void 0);
__decorate([
    graphql_1.Field(),
    typeorm_1.Column(),
    typeorm_1.CreateDateColumn(),
    __metadata("design:type", Date)
], InvoiceModel.prototype, "createdAt", void 0);
__decorate([
    graphql_1.Field(),
    typeorm_1.Column(),
    typeorm_1.UpdateDateColumn(),
    __metadata("design:type", Date)
], InvoiceModel.prototype, "updatedAt", void 0);
InvoiceModel = __decorate([
    graphql_1.ObjectType(),
    typeorm_1.Entity()
], InvoiceModel);
exports.InvoiceModel = InvoiceModel;
//# sourceMappingURL=invoice.model.js.map