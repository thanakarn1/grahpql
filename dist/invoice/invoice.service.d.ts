import { CustomerService } from './../customer/customer.service';
import { InvoiceModel } from './invoice.model';
import { Repository } from 'typeorm';
import { CreateInvoiceDTO } from './invoice.dto';
export declare class InvoiceService {
    private invoiceRepository;
    private customerService;
    constructor(invoiceRepository: Repository<InvoiceModel>, customerService: CustomerService);
    create(invoice: CreateInvoiceDTO): Promise<InvoiceModel>;
    findAll(): Promise<InvoiceModel[]>;
    findByCustomer(id: string): Promise<InvoiceModel[]>;
    findOne(id: string): Promise<InvoiceModel>;
}
