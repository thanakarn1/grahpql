"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.InvoiceService = void 0;
const customer_service_1 = require("./../customer/customer.service");
const invoice_model_1 = require("./invoice.model");
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
let InvoiceService = class InvoiceService {
    constructor(invoiceRepository, customerService) {
        this.invoiceRepository = invoiceRepository;
        this.customerService = customerService;
    }
    async create(invoice) {
        const customer = await this.customerService.findOne(invoice.customer);
        const subTotal = invoice.items.reduce((acc, curr) => {
            return acc + Number((curr.rate * curr.quantity).toFixed(2));
        }, 0);
        const taxAmount = subTotal * Number((invoice.taxRate / 100).toFixed(2));
        const total = subTotal + taxAmount;
        const outstandingBalance = total;
        return this.invoiceRepository.save({
            ...invoice,
            customer,
            subTotal,
            taxAmount,
            total,
            outstandingBalance
        });
    }
    findAll() {
        return this.invoiceRepository.find();
    }
    findByCustomer(id) {
        return this.invoiceRepository.createQueryBuilder("invoice")
            .where("invoice.customer = :id", { id })
            .getMany();
    }
    findOne(id) {
        return this.invoiceRepository.findOne(id);
    }
};
InvoiceService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(invoice_model_1.InvoiceModel)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        customer_service_1.CustomerService])
], InvoiceService);
exports.InvoiceService = InvoiceService;
//# sourceMappingURL=invoice.service.js.map