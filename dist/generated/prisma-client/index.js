"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.prisma = exports.Prisma = exports.models = void 0;
const prisma_client_lib_1 = require("prisma-client-lib");
const prisma_schema_1 = require("./prisma-schema");
exports.models = [
    {
        name: "User",
        embedded: false
    }
];
exports.Prisma = prisma_client_lib_1.makePrismaClientClass({
    typeDefs: prisma_schema_1.typeDefs,
    models: exports.models,
    endpoint: `https://us1.prisma.sh/thanakarn/nest-graphql/dev`
});
exports.prisma = new exports.Prisma();
//# sourceMappingURL=index.js.map